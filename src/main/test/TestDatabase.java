import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import toDoRestServer.database.ToDoJDBC;
import toDoRestServer.pojo.Person;
import toDoRestServer.pojo.ToDo;

import java.time.LocalDate;
import java.util.Map;

public class TestDatabase {
    private ToDoJDBC databaseTest;
    private ToDo todoOne;
    private ToDo todoTwo;
    private ToDo todoThree;
    private ToDo todoFour;

    @Before
    public void init() {
        databaseTest = new ToDoJDBC();
        Person personOne = new Person("Olga", "Fedorova", "Evgenevna");
        Person personTwo = new Person("Ivan", "Ivanov", "Ivanovich");
        LocalDate dateOne = LocalDate.of(2018, 02, 27);
        LocalDate dateTwo = LocalDate.of(2018, 02, 26);
        LocalDate dateThree = LocalDate.of(2018, 03, 03);
        LocalDate dateFour = LocalDate.of(2018, 03, 01);
        todoOne = new ToDo(dateTwo, "Test service", personOne);
        todoTwo = new ToDo(dateOne, "Finish service", personOne);
        todoThree = new ToDo(dateFour, "Say \"Hello, world!\"", personTwo);
        todoFour = new ToDo(dateThree, "Write mail to world", personTwo);
        databaseTest.deleteAll();
        databaseTest.addPerson(personOne);
        databaseTest.addPerson(personTwo);
        databaseTest.addToDo(todoOne);
        databaseTest.addToDo(todoThree);
        databaseTest.addToDo(todoFour);
    }

    @Test
    public void testGetAll() {
        Assert.assertTrue(databaseTest.getToDoList().containsValue(todoOne));
        Assert.assertTrue(databaseTest.getToDoList().containsValue(todoThree));
        Assert.assertTrue(databaseTest.getToDoList().containsValue(todoFour));
    }

    @Test
    public void testAddNegative() {
        Assert.assertTrue(!databaseTest.getToDoList().containsValue(todoTwo));
    }

    @Test
    public void testAdd() {
        databaseTest.addToDo(todoOne);
        databaseTest.addToDo(todoTwo);
        databaseTest.addToDo(todoThree);
        databaseTest.addToDo(todoFour);
        Assert.assertTrue(databaseTest.getToDoList().containsValue(todoTwo));
    }

    @Test
    public void testDeleteAll() {
        databaseTest.deleteAll();
        Assert.assertEquals(databaseTest.getToDoList().size(), 0);
    }

    @Test
    public void testRemove() {
        databaseTest.getToDoList().forEach((key, value) -> {
            if (value.equals(todoTwo)) {
                databaseTest.removeToDo(key);
            }
        });
        Assert.assertTrue(!databaseTest.getToDoList().containsValue(todoTwo));
    }

    @Test
    public void testRemoveNegative() {
        databaseTest.getToDoList().forEach((key, value) -> {
            if (value.equals(todoOne)) {
                databaseTest.removeToDo(key);
            }
        });
        Assert.assertTrue(!databaseTest.getToDoList().containsValue(todoOne));
    }

    @Test
    public void refreshToDateTest() {
        LocalDate test = LocalDate.of(2019, 11, 2);
        databaseTest.getToDoList().forEach((key, value) -> {
            if (value.equals(todoOne) && !value.getDate().equals(test)) {
                databaseTest.refreshToDoDate(key, test);
                Assert.assertEquals(databaseTest.getToDo(key).getDate(), test);
            }
        });
    }

    @Test
    public void refreshToDateTestNegative() {
        LocalDate test = LocalDate.of(2019, 11, 2);
        databaseTest.getToDoList().forEach((key, value) -> {
            if (value.equals(todoOne) && !value.getDate().equals(test)) {
                databaseTest.refreshToDoDate(key, test);
                Assert.assertNotEquals(databaseTest.getToDo(key), todoOne);
            }
        });
    }


    @Test
    public void refreshToPersonTest() {
        Person test = new Person("Ivan", "Ivanov", "Ivanovich");
        databaseTest.getToDoList().forEach((key, value) -> {
            if (value.equals(todoOne) && !value.getPerson().equals(test)) {
                databaseTest.refreshToDoPerson(key, test);
                Map<Integer,ToDo> toDoList = databaseTest.getToDoList();
                Assert.assertTrue(!toDoList.containsValue(todoOne));
            }
        });
    }

    @Test
    public void refreshToPersonTestNegative() {
        Person test = new Person("Ivan", "Ivanov", "Ivanovich");
        databaseTest.getToDoList().forEach((key, value) -> {
            if (value.equals(todoTwo) && !value.getPerson().equals(test)) {
                databaseTest.refreshToDoPerson(key, test);
                Assert.assertTrue(databaseTest.getToDoList().containsValue(todoOne));
            }
        });
    }

    @Test
    public void refreshToMessageTest() {
        String test = "Say \"Hello, world!\"";
        databaseTest.getToDoList().forEach((key, value) -> {
            if (value.equals(todoFour) && !value.getPerson().equals(test)) {
                databaseTest.refreshToDoMessage(key, test);
                Assert.assertEquals(databaseTest.getToDo(key).getMessage(), test);
            }
        });
    }

    @Test
    public void refreshToMessageTestNegative() {
        String test = "Say \"Hello, world!\"";
        databaseTest.getToDoList().forEach((key, value) -> {
            if (value.equals(todoFour) && !value.getPerson().equals(test)) {
                databaseTest.refreshToDoMessage(key, test);
            }
            Assert.assertNotEquals(databaseTest.getToDo(key), todoFour);
        });
    }
}
