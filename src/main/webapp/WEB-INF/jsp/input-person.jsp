<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<head>
	<meta charset="UTF-8">
	<title>To-do</title>
	<link rel="stylesheet" type="text/css" href="<c:url value="/bootstrap/css/bootstrap.css"/>">
	<link rel="stylesheet" type="text/css" href="<c:url value="/bootstrap/css/style-input-person.css"/>">
	<script
        	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
     <link rel="stylesheet" type="text/javascript" href="<c:url value="/bootstrap/js/bootstrap.js"/>">
</head>
<body>
<div class="container">
  <div class="info">
 <div class="header">
   <h2>Enter your personal information: </h2>
</div>
<div class="form">
<div class="main">
<div class="field">
 <p><label>Firstname</label><input type="text" id ="firstName"/></p>
</div>
<div class="field">
   <p><label>Secondname</label><input type="text" id ="secondName"/></p>
</div>
<div class="field">
   <p><label>Patronymic</label><input type="text" id ="patronymic"/></p>
</div>
<div class="button">
   <button onclick="sendPerson()" id=1 type="button" class="btn btn-primary"><span class="glyphicon glyphicon-send"></span> Send</button>
</div>
</div>
</div>
 </div>
 </div>

<script type="text/javascript">
        function sendPerson() {
        var error = 0;
        var firstNameLocal = $("#firstName").val();
        var secondNameLocal = $('#secondName').val();
        var patronymicLocal = $('#patronymic').val();
                    $("body").find(":input").each(function() {
                    if(!$(this).val() && this.id!=1) {
                       $(this).css('border', 'red 2px solid');
                       error = 1;
                     }
                     else{
                       $(this).css('border', 'gray 1px solid');
                     }
                   });
                   if (error == 1) {
                   return false;
                   }
                   var local= document.location.href;
                   var id  = local.split("id=")[1];
                  $.ajax({
                  		url : '/refresh/person',
                  		datatype : 'json',
                  		contentType : "application/json",
                  		type : "post",
                  		data : JSON.stringify({
                  			toDoId : id,
                  			person : {
                  			firstName : firstNameLocal,
                            secondName : secondNameLocal,
                            patronymic : patronymicLocal
                            }
                  		}),
                  		success : function(data) {
                  		 $(location).attr('href','/menu');
                  		}
                  	});
              }
        </script>

</body>
</html>