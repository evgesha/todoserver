<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page isELIgnored = "false" %>
<%@ page import = "toDoRestServer.pojo.ToDo,toDoRestServer.pojo.Person,java.util.*" %>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<head>
	<meta charset="UTF-8">
	<title>To-do</title>
	<link rel="stylesheet" type="text/css" href="<c:url value="/bootstrap/css/bootstrap.css"/>">
	<link rel="stylesheet" type="text/css" href="<c:url value="/bootstrap/css/style-result.css"/>">
	<script
    	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
      <link rel="stylesheet" type="text/javascript" href="<c:url value="/bootstrap/js/bootstrap.js"/>">
</head>
<body>
<div class="container">
<div class="row">
<div class="col-lg-12 col-md-8 col-sm-10 col-xs-12">
<div class="table-responsive">
        <table id="table" class="table">
        </table>
</div>
</div>
<div class="col-lg-12 col-md-4 col-sm-2 col-xs-12">
        <div class="field">
            <a href='add'>Add task</a>
        </div>
</div>
</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
    var tr = $('<tr />', {}).appendTo('table');
    $('<th>Name</th>').appendTo(tr);
    $('<th>Date</th>').appendTo(tr);
    $('<th>Text</th>').appendTo(tr);
$.ajax({
		url : '/all/task',
		datatype : 'json',
		success : function(data) {
		createTable(data);
		}
	});
});

function createTable(data) {
		var name, text, date, tr, href1, href2, href3;
		for (var key in data) {
		name = data[key]["person"]["firstName"] + " " + data[key]["person"]["secondName"] + " " + data[key]["person"]["patronymic"];
		date = data[key]["date"]["year"] +  "-" + data[key]["date"]["monthValue"] + "-" + data[key]["date"]["dayOfMonth"];
		text = data[key]["message"];
		href1 = '<a href="/person?id=' + key + '">' + name + "</a>";
		href2 = '<a href="/date?id=' + key + '">' + date + "</a>";
		href3 = '<a href="/text?id=' + key + '">' + text + "</a>";
		tr = $('<tr />', {}).appendTo('table');
		$('<td>' + href1 + '</td>').appendTo(tr);
		$('<td>' + href2 + '</td>').appendTo(tr);
		$('<td>' + href3 + '</td>').appendTo(tr);
		$('<button />', {
		id: key,
		html: "&#10006;",
		click: function() {
                       remove(this);
                     }
		}).appendTo(tr);
	}
}

function remove(object) {
var url = '/remove/task/' + object.id;
$.ajax({
 url : url,
 datatype : 'json',
 contentType : "application/json",
 type : "post",
 success : function(data) {
if (typeof(object) == "object") {
        $(object).closest("tr").remove();
    } else {
        return false;
    }
 }
 });
}
</script>

</body>
</html>