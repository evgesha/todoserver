<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<head>
	<meta charset="UTF-8">
	<title>To-do</title>
	<link rel="stylesheet" type="text/css" href="<c:url value="/bootstrap/css/bootstrap.css"/>">
	<link rel="stylesheet" type="text/css" href="<c:url value="/bootstrap/css/style-input-date.css"/>">
	<script
    	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/javascript" href="<c:url value="/bootstrap/js/bootstrap.js"/>">
</head>
<body>
<div class="container">
 <div class="header">
   <p>Select a date: </p>
</div>
   <p><input type="date" id="date"></p>
   <p><button onclick="sendDate()"type="button" class="btn btn-primary"><span class="glyphicon glyphicon-send"></span> Send</button></p>
</div>

     <script type="text/javascript">
     function sendDate(){
      var dateLocal = $("#date").val();
              if (dateLocal .length==0){
                  $("#date").css('border', 'red 2px solid');
                   return false;
                }
                else {
                 $("#date").css('border', 'gray 1px solid');
                 }
                var local = document.location.href;
                var id  = local.split("id=")[1];
               $.ajax({
               		url : '/refresh/date',
               		datatype : 'json',
               		type : "post",
               		contentType : "application/json",
               		data : JSON.stringify({
               		todoId : id,
                    date : dateLocal
                    }),
               		success : function(data) {
               			 $(location).attr('href','/menu');
               		}
               	});
           }
     </script>\
</body>
</body>
</html>