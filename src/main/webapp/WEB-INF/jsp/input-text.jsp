<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<head>
	<meta charset="UTF-8">
	<title>To-do</title>
	<link rel="stylesheet" type="text/css" href="<c:url value="/bootstrap/css/bootstrap.css"/>">
	<link rel="stylesheet" type="text/css" href="<c:url value="/bootstrap/css/style-input-text.css"/>">
	<script
    	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/javascript" href="<c:url value="/bootstrap/js/bootstrap.js"/>">
</head>
<body>
<div class="container">
   <p>Enter the text of the task: </p>
   <p><textarea id="text" ></textarea></p>
   <button onclick="sendText()" type="button" class="btn btn-primary"><span class="glyphicon glyphicon-send"></span> Send</button>
</div>

<script type="text/javascript">
function sendText() {
var textLocal =  $("#text").val();
         if (textLocal.length==0){
              $("#text").css('border', 'red 2px solid');
              return false;
           }
           else {
           $("#text").css('border', 'gray 1px solid');
           }
           var local = document.location.href;
           var id  = local.split("id=")[1];
                  $.ajax({
                  		url : '/refresh/message',
                  		datatype : 'json',
                  		contentType : "application/json",
                  		type : "post",
                  		data : JSON.stringify({
                  			todoId : id,
                  			message : textLocal
                  		}),
                  		success : function(data) {
                  		   $(location).attr('href','/menu');
                  		}
                  	});
}
</script>
</body>
</html>