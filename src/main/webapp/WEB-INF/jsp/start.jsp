<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<head>
	<meta charset="UTF-8">
	<title>To-do</title>
	<link rel="stylesheet" type="text/css" href="<c:url value="/bootstrap/css/bootstrap.css"/>">
	<link rel="stylesheet" type="text/css" href="<c:url value="/bootstrap/css/style-greeting.css"/>">
	<script
    	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
     <link rel="stylesheet" type="text/javascript" href="<c:url value="/bootstrap/js/bootstrap.js"/>">
</head>
<body>
<div class="container">
       <h2>Welcome to-do server!</h2>
       <button onclick="getPage()" type="button" class="btn btn-primary"><span class="glyphicon glyphicon-send"></span> Enter</button>
</div>

<script type="text/javascript">
  function getPage() {
            document.location.href='/menu';
        }
  </script>
</body>
</html>