package toDoRestServer.server;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import toDoRestServer.database.ToDoJDBC;
import toDoRestServer.domain.*;
import toDoRestServer.pojo.ToDo;

import java.util.Map;

@CrossOrigin(origins = "http://localhost:8080")
@RestController
public class ControllerService {
    private ToDoJDBC database = new ToDoJDBC();

    @RequestMapping("/main")
    @ResponseBody
    public String welcome() {
        return "Welcome to Rest on Spring";
    }

    @RequestMapping(value = "/all/task", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Map<Integer, ToDo>> getToDoList() {return new ResponseEntity<>(database.getToDoListForJson(), HttpStatus.OK);
    }

    @PostMapping(value = "/refresh/message", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public boolean refreshToDoMessage(@RequestBody ToDoOfMessage toDoOfMessage) {
        return database.refreshToDoMessage(toDoOfMessage.getTodoId(), toDoOfMessage.getMessage());
    }

    @PostMapping(value = "/refresh/date", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public boolean refreshToDoDate(@RequestBody ToDoOfDates toDoOfDate) {
       return database.refreshToDoDate(toDoOfDate.getTodoId(), toDoOfDate.getDateLocal());
    }

    @PostMapping(value = "/refresh/person", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public boolean refreshToDoPerson(@RequestBody ToDoOfPerson toDoOfPerson) {
        return database.refreshToDoPerson(toDoOfPerson.getToDoId(), toDoOfPerson.getPerson());
    }

    @PostMapping(value = "/remove/task/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void removeToDo(@PathVariable("id") int id) {
        database.removeToDo(id);
    }

    @PostMapping(value = "/add/task", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void addToDo(@RequestBody ToDoInfo toDo) {
        database.addToDo(toDo.getToDo());
    }
}
