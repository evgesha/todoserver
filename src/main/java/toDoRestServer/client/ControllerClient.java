package toDoRestServer.client;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ControllerClient {
	
	@GetMapping("/")
	public String getStartPage(){
		return "start";
	}

	@GetMapping("/add")
	public String addToDo(){
		return "input-todo";
	}

	@GetMapping("/person")
	public String refreshPerson(){
		return "input-person";
	}

	@GetMapping("/text")
	public String refreshText(){
		return "input-text";
	}

	@GetMapping("/date")
	public String refreshDate(){
		return "input-date";
	}

	@GetMapping("/menu")
	public String getMenu(){
		return "menu";
	}
}
