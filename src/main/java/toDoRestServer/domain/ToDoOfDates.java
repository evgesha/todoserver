package toDoRestServer.domain;

import java.time.LocalDate;
import java.util.Date;

public class ToDoOfDates {
    private int todoId;
    private String date;

    public ToDoOfDates() {
        this.date = new String();
    }

    public ToDoOfDates(int todoId, String date) {
        this.todoId = todoId;
        this.date = date;
    }

    public int getTodoId() {
        return todoId;
    }

    public void setTodoId(int todoId) {
        this.todoId = todoId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public LocalDate getDateLocal() {
        return LocalDate.parse(date);
    }
}
