package toDoRestServer.domain;

import toDoRestServer.pojo.Person;
import toDoRestServer.pojo.ToDo;

import java.time.LocalDate;

public class ToDoInfo {
    private Person person;
    private String date;
    private String text;

    public ToDoInfo() {
        this.person = new Person();
        this.date = new String();
        this.text = new String();
    }

    public ToDoInfo(Person person, String date, String text) {
        this.person = person;
        this.date = date;
        this.text = text;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public ToDo getToDo() {
        return new ToDo(LocalDate.parse(date), text, person);
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }
}
